import * as React from 'react';
import * as ReactDom from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Overview from './components/Overview';
import AddDictionary from './components/Dictionary/components/Add/AddDictionary';
import DictionaryStore from './stores/DictionaryStore';
import { Provider } from 'mobx-react';
import EditDictionary from './components/Dictionary/components/Edit/EditDictionary';

const dictionaryStore = new DictionaryStore();

ReactDom.render(
    <Provider dictionaryStore={dictionaryStore}>
        <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Dictionaries Overview</Link>
                        </li>
                        <li>
                            <Link to="/dictionary/add">Add Dictionary</Link>
                        </li>
                    </ul>
                </nav>
                <Route path="/" exact component={Overview} />
                <Route path="/dictionary/add" component={AddDictionary} />
                <Route path="/dictionary/edit/:id" component={EditDictionary} />
            </div>
        </Router>
    </Provider>
    ,
  document.getElementById('root')
);
