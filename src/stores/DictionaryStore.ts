import { observable, action } from 'mobx';
import {
  IProduct,
  IDictionary,
} from '../interfaces';

class DictionaryStore {

  @observable public dictionary: IDictionary[] = [
    ['Anthracite', 'Dark Grey'],
    ['Midnight Black', 'Black'],
    ['Mystic Silver', 'Silver'],
  ];

  @observable public products: IProduct[] = [
    { Product: 'Apple iPhone 6s', Color: this.dictionary[0], Price: 272 },
    { Product: 'Samsung Galaxy S8', Color: this.dictionary[1], Price: 272 },
    { Product: 'Apple iPhone 6s', Color: this.dictionary[2], Price: 272 },
  ];

  //constructor() {}

  public getProduct = (product: IProduct) => {
    return this.products.find(p =>
      p.Product === product.Product &&
      p.Price === product.Price &&
      p.Color === product.Color) || null;
  }

  public getProductByProductName = (productName: string) => {
    return this.products.find(p => p.Product === productName) || null;
  }

  @action
  public addProduct = (product: IProduct) => {
    this.products.push(product);
  }

  public replaceProduct = (productName: string, product: IProduct) => {
    const prodIndex = this.products.findIndex(p => p.Product === productName);
    if (prodIndex > -1) {
      this.products[prodIndex] = product;
    }
  }

  public removeProduct = (product: IProduct) => {
    const prodIndex = this.products.findIndex(p =>
      p.Product === product.Product &&
      p.Price === product.Price &&
      p.Color === product.Color);
    if (prodIndex > -1) {
      this.products.splice(prodIndex);
    }
  }

  public getDictionaryByKey = (key: string) => {
    const dict = this.dictionary.find(d => d[0] === key);
    return dict || null;
  }

  @action
  public addDictionary = (newDictionary: IDictionary) => {
    if (!this.getDictionaryByKey(newDictionary[0])) {
      this.dictionary.push(newDictionary);
    }
  }

  @action
  public replaceDictionary = (fromColor: string, toColor: string) => {
    const dictItem = this.getDictionaryByKey(fromColor);
    if (dictItem) {
      dictItem[1] = toColor;
    }
  }

  @action
  public removeDictionary = (index: number) => {
    this.dictionary.splice(index, 1);
  }
}

export default DictionaryStore;
