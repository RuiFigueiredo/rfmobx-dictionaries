import DictionaryStore from './stores/DictionaryStore';

export interface IProduct {
    Product: string;
    Color: IDictionary;
    Price: number;
}

export interface IDictionary {
    0: string;
    1: string;
}

export interface IDictionaryStoreProps {
    dictionaryStore: DictionaryStore;
}

export interface IDictionaryProps {
    dictionary: IDictionary;
    action: Function;
    actionTitle?: string;
}

export interface IRouterProps {
    history: any;
}

export interface IRouterProps {
    history: any;
}
