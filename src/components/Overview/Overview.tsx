import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { Grid, Col, Row, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Products from '../Products/Products';
import { action } from 'mobx';

@inject('dictionaryStore')
@observer
class Overview extends React.Component<any, {}> {

  constructor(p, c) {
    super(p, c);
  }

  private addTransformation = () => {
    return(
        <Link to="/dictionary/add">Add Dictionary Transformation</Link>
    );
  }

  @action
  private removeItem = event => {
    const index = parseInt(event.currentTarget.dataset.index, 10);
    this.props.dictionaryStore.removeDictionary(index);
  }

  private renderTableHeader = () => {
    return(
        <Row>
            <Col xs={4}>
                From
            </Col>
            <Col xs={4}>
                To
            </Col>
            <Col xs={4} />
        </Row>
    );
  }

  private renderTable = () => {
    return (
        this.props.dictionaryStore &&
        this.props.dictionaryStore.dictionary &&
        this.props.dictionaryStore.dictionary.length ?
        this.props.dictionaryStore.dictionary.map((dict, index) => {
            return(
                <Row key={index}>
                    <Col xs={4}>
                        {dict[0]}
                    </Col>
                    <Col xs={4}>
                        {dict[1]}
                    </Col>
                    <Col xs={4}>
                        <Link to={'/dictionary/edit/' + dict[0]}>Edit</Link>
                        <Button data-index={index} onClick={this.removeItem}>Remove</Button>
                    </Col>
                </Row>
            );
        })
        : null
      );
  }

  public render() {
    return (
        <React.Fragment>
            <h1>Dictionary Transformation</h1>
            <Grid>
                {this.addTransformation()}
                {this.renderTableHeader()}
                {this.renderTable()}
            </Grid>
            <hr/>
            <Products dictionaryStore={this.props.dictionaryStore} />
        </React.Fragment>
    );
  }
}

export default Overview;
