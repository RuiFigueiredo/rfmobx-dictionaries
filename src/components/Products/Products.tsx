import * as React from 'react';
import { observer } from 'mobx-react';
import { Grid, Col, Row } from 'react-bootstrap';
import { IDictionaryStoreProps } from 'src/interfaces';

class Products extends React.Component<IDictionaryStoreProps, {}> {

    constructor(p, c) {
        super(p, c);
    }

    private renderTableHeader = () => {
        return (
            <Row>
                <Col xs={3}>
                    Product
            </Col>
                <Col xs={2}>
                    Color
            </Col>
                <Col xs={2}>
                    Transformation Color
            </Col>
                <Col xs={2}>
                    Price
            </Col>
                <Col xs={3} />
            </Row>
        );
    }

    private renderTable = () => {
        const { products } = this.props.dictionaryStore;
        return (
            products &&
                products.length ?
                products.map((prod, index) => {
                    return (
                        <Row key={index}>
                            <Col xs={3}>
                                {prod.Product}
                            </Col>
                            <Col xs={2}>
                                {prod.Color['0']}
                            </Col>
                            <Col xs={2}>
                                {prod.Color['1']}
                            </Col>
                            <Col xs={2}>
                                CHF {prod.Price}
                            </Col>
                            <Col xs={3}>
                                edit | delete
                    </Col>
                        </Row>
                    );
                })
                : null
        );
    }

    public render() {
        return (
            <div>
                <h2>Products</h2>
                <Grid>
                    {this.renderTableHeader()}
                    {this.renderTable()}
                </Grid>
            </div>
        );
    }
}

export default observer(Products);
