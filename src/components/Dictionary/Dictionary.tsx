import * as React from 'react';
import { Button } from 'react-bootstrap';
import { IDictionaryProps } from 'src/interfaces';
import { observer } from 'mobx-react';

@observer
class Dictionary extends React.Component<IDictionaryProps, {}> {

  constructor(p, c) {
    super(p, c);

    this.onChangeFromColor = this.onChangeFromColor.bind(this);
    this.onChangeToColor = this.onChangeToColor.bind(this);
  }

  private onChangeFromColor = event => {
    this.props.dictionary['0'] = event.target.value;
  }

  private onChangeToColor = event => {
    this.props.dictionary['1'] = event.target.value;
  }

  private performAction = () => this.props.action();

  public render() {
    const { dictionary, actionTitle } = this.props;
    return (
        <form>
            From Color:
            <input type="text" name="fromColor" value={dictionary['0']} onChange={this.onChangeFromColor}/>

            To Color:
            <input type="text" name="toColor" value={dictionary['1']} onChange={this.onChangeToColor}/>

            <Button onClick={this.performAction}>{actionTitle}</Button>
        </form>
    );
  }
}

export default Dictionary;
