import * as React from 'react';
import { inject, observer } from 'mobx-react';
import { Grid } from 'react-bootstrap';
import { IDictionaryStoreProps, IDictionary, IRouterProps } from 'src/interfaces';
import { withRouter, Link } from 'react-router-dom';
import Dictionary from '../../Dictionary';
import { observable } from 'mobx';
import DictionaryStore from '../../../../stores/DictionaryStore';

@inject('dictionaryStore')
@observer
class AddDictionary extends React.Component<IRouterProps & IDictionaryStoreProps & DictionaryStore, {}> {

//  @observable private dictionaryStore: DictionaryStore = new DictionaryStore();
  @observable private dictionary: IDictionary = ['', ''];

  constructor(p, c) {
    super(p, c);
  }

  private addDictionary = () => {
    this.props.dictionaryStore.addDictionary(this.dictionary);
    this.props.history.push('/');
  }

  public render() {
    return (
      <div>
        <h2>Add Dictionary</h2>
        <Grid>
            <Dictionary dictionary={this.dictionary} actionTitle={'Add'} action={this.addDictionary} />
        </Grid>
        <Link to="/">Back to Overview</Link>
      </div>
    );
  }
}

export default withRouter(AddDictionary);
