import * as React from 'react';
import { inject, observer } from 'mobx-react';
import { Grid } from 'react-bootstrap';
import { IDictionaryStoreProps, IDictionary, IRouterProps } from 'src/interfaces';
import { withRouter, Link, match } from 'react-router-dom';
import Dictionary from '../../Dictionary';
import { observable } from 'mobx';

@inject('dictionaryStore')
@observer
class EditDictionary extends React.Component<match & IRouterProps & IDictionaryStoreProps, {}> {

  @observable private dictionary: IDictionary;

  constructor(p, c) {
    super(p, c);
    this.dictionary = this.props.dictionaryStore.getDictionaryByKey(this.props.match.params.id);
  }

  private editDictionary = () => {
    this.props.dictionaryStore.replaceDictionary(this.dictionary[0], this.dictionary[1]);
    this.props.history.push('/');
  }

  public render() {
    return (
      <div>
        <h2>Edit Dictionary</h2>
        <Grid>
            <Dictionary dictionary={this.dictionary} actionTitle={'Change'} action={this.editDictionary} />
        </Grid>
        <Link to="/">Back to Overview</Link>
      </div>
    );
  }
}

export default withRouter(EditDictionary);
