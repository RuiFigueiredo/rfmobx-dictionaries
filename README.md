# RFMobx

Initial run:

* `yarn install`
* `yarn start`

Dependencies:
.Typescript
.NodeJS
.jest

Running tests:
* yarn test
* jest
* npm t -- --watch
