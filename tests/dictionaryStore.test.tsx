import DictionaryStore from '../src/stores/DictionaryStore';
import { IDictionary } from 'src/interfaces';

const store = new DictionaryStore();

describe('DictionaryStore', () => {
  it('creates/add new dictionaries', () => {
    expect(store.dictionary.length).toBe(3); //initial 3 for sample purpose
    store.addDictionary(['red', '#ff0000']);
    store.addDictionary(['blue', '#0000ff']);
    expect(store.dictionary.length).toBe(5);
    expect(store.dictionary[3][0]).toBe('red');
    expect(store.dictionary[3][1]).toBe('#ff0000');
  });

  it('avoid duplicated keys insertion/forks', () => {
    const anotherRedItem: IDictionary = ['red', 'another red'];
    store.addDictionary(anotherRedItem);
    const redItemsInDictionary = store.dictionary.filter(d =>
      d[0] === anotherRedItem[0]);
    expect(store.getDictionaryByKey(anotherRedItem[0])).not.toBeNull();
    expect(redItemsInDictionary.length).toBe(1);
    expect(store.dictionary.length).toBe(5);
  });

  it('edit existing dictionary', () => {
    expect(store.getDictionaryByKey('red')).toBeTruthy();
    store.replaceDictionary('red', 'rgba(255,0,0,1)');
    const redItem: IDictionary | null = store.getDictionaryByKey('red');
    expect(redItem).not.toBeNull();
    expect(redItem && redItem[0]).toBe('red');
    expect(redItem && redItem[1]).toBe('rgba(255,0,0,1)');
  });

  it('delete dictionaries', () => {
    expect(store.dictionary.length).toBe(5);
    store.removeDictionary(3);
    const redItem: IDictionary | null = store.getDictionaryByKey('red');
    expect(store.dictionary.length).toBe(4);
    expect(redItem).toBeNull();
  });

  it('avoid clones in the dictionary', () => {
    store.dictionary.forEach(dict => {
      expect(store.dictionary.filter(d =>
        d[0] === dict[0] && d[1] === dict[1]).length).not.toBeGreaterThan(1);
    });
  });

  //TBD: delete a dictionary and change behavior of the Product

});
